#!/bin/bash
# vim: dict=/usr/share/rhts-library/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/dmidecode/Sanity/basic-sanity-check
#   Description: Just executes binaries and checks for right exitcode.
#   Author: Matej Susta <msusta@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2009 Red Hat, Inc. All rights reserved.
#   
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#   
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#   
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include rhts environment
. /usr/bin/rhts-environment.sh
. /usr/share/rhts-library/rhtslib.sh

PACKAGE="dmidecode"

rlJournalStart
    rlPhaseStartSetup Setup
        rlAssertRpm $PACKAGE
	if [ "`uname -p`" = "ia64" ]; then
		IA64=1
	fi
    rlPhaseEnd

    rlPhaseStartTest Testing
	(($IA64)) || rlRun "biosdecode" 0 "biosdecode"
	rlRun "dmidecode >$TmpDir/normal" 0 "dmidecode without args"
	grep "OUT OF SPEC" "$TmpDir/normal" && rlLogWarning "OUT OF SPEC issue, see bug #714591 and bug #714595"
	
	rlAssertNotGrep "BAD INDEX" "$TmpDir/normal"
		# DBG:
		echo "##### NORMAL #####"
		cat $TmpDir/normal
	rlRun "dmidecode --quiet >$TmpDir/quiet" 0 "dmidecode in quieter mode"
	rlRun "diff $TmpDir/normal $TmpDir/quiet" 1 "quiet should be quiet"
	rlRun "dmidecode --type 0 >$TmpDir/type" 0 "dmidecode for type"
	rlRun "diff $TmpDir/normal $TmpDir/type" 1 "type output check"
#	rlRun "dmidecode --dump --quiet" 2 "dmidecode shouldn't like dump+quiet"
	(($IA64)) || rlRun "ownership" 0 "ownership"
	(($IA64)) || rlRun "vpddecode" 0 "vpddecode"
    rlPhaseEnd
rlJournalPrintText
